hw2: hw2.o
	gcc -o hw2 -g hw2.o -lcrypto

hw2.o: hw2.c
	gcc -g -c -Wall hw2.c -I/usr/local/opt/openssl/include

clean:
	rm -f *.o hw2
