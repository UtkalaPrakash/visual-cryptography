#include<stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include <getopt.h>
#include <math.h>

#define MD5_DIGEST_LENGTH 16

void pusage() {
	// This prints the usage and ends the program on occurrence of error.
	fprintf(stderr, "Malformed command\n");
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "\t./hw2 stream -p=pphrase -l=len\n");
	fprintf(stderr, "\t./hw2 encrypt -p=pphrase -out=name [pbmfile]\n");
	fprintf(stderr, "\t./hw2 merge pbmfile1 pbmfile2\n");
	fprintf(stderr, "\t./hw2 decrypt [pbmfile]\n");
	exit(1);
}

FILE* fileCheck(char *file_name) {
	// This function checks whether input file is readable and its existence.
	if ( access(file_name, F_OK) == -1 ) {
	    fprintf(stderr, "Input file %s doesn't exist\n", file_name);
	    exit(1);
	}

	struct stat file_check;
	stat(file_name, &file_check);
	if ( S_ISDIR(file_check.st_mode) ) {
	    fprintf(stderr, "Input file %s is a directory\n", file_name);
	    exit(1);
	}

	if ( access(file_name, R_OK) == -1 ) {
	    fprintf(stderr, "Cannot read input file %s, permission denied\n", file_name);
	    exit(1);
	}

	FILE *file_handler = fopen(file_name, "r");
	if ( file_handler == NULL) {
            fprintf(stderr, "Input file %s cannot be open\n", file_name);
            exit(1);
        }
	return file_handler;
}

unsigned char* generate_key (char *pphrase, int len) {
	// Modified the piece of  code given in the homework
	char md5_buf[MD5_DIGEST_LENGTH];
	int l = strlen(pphrase) + 2 + MD5_DIGEST_LENGTH;
	char *s = malloc(l+1);
	if (len%8 != 0) len = ((len/8) + 1) * 8;
	int i = 0, length = len;
	unsigned char *key = malloc(len+1);

	MD5((unsigned char*) pphrase, strlen(pphrase), (unsigned char*) md5_buf);
	while(len > 0) {
		if ( i == 100 ) i = 0;
    		sprintf(&s[MD5_DIGEST_LENGTH], "%02d%s", i, pphrase);
    		memcpy(s, md5_buf, MD5_DIGEST_LENGTH);
    		MD5((unsigned char*) s, l, (unsigned char*) md5_buf);
			memcpy(key, md5_buf, 8);
			key += 8;
			len -= 8;
			i++;
    	}
    	*key = '\0';
    	key -= length;
    	free(s);
    	return(key);
}

int main(int argc, char **argv) {
    char option[8] = "";
	char pphrase[100], name[100];
	int length, opt;

	if (argc < 2 ) pusage();
    // Using my code from warmup assignments in CSCI 402
    if (	strcmp(*(argv+1), "stream") != 0 && 
			strcmp(*(argv+1), "encrypt") != 0 && 
			strcmp(*(argv+1), "merge") != 0 && 
			strcmp(*(argv+1), "decrypt") != 0 ) {
	    pusage();
    } else {
	    strcpy(option, *(argv+1));
	}

	static struct option options[] = {
        	{"p",    required_argument, 0,  'p' },
        	{"l",    required_argument, 0,  'l' },
        	{"out",   required_argument, 0,  'o' },
			{NULL, 0, NULL, 0}
	};

	int index = 0;
	// Handles input arguments
	while ((opt = getopt_long_only(argc, argv,"pl:o:", options, &index)) != -1) {
		if ( opt == 'p' ) {
		    sprintf(pphrase, "%s", optarg);
		} else if ( opt == 'l' ) {
		    length = atoi(optarg);
		} else if ( opt == 'o' ) {
		    sprintf(name, "%s", optarg);
		} else {
	 	    pusage();
		}
    }

	if ( strcmp(option, "stream") == 0 ) {
			if (argc != 4) pusage();
		if (strlen(pphrase) == 0 || length == 0) pusage(); 
        	unsigned char *key = generate_key(pphrase, length);
        	int i = 0;
        	// Prints required number of bytes byte-wise
        	while (i<length) {
        		fprintf(stdout, "%c",*key);
				fflush(stdout);
        		i++;
        		key++;
        	}
	} else if ( strcmp(option, "encrypt") == 0 ) {
	    	FILE *fRead;
	    	// First check for file existence and access before proceeding
	    	if (argc < 4) pusage();
		if ( strlen(pphrase) == 0 || strlen(name) == 0 ) pusage();
	    	if ( argc == 5 ) {
				fRead = fileCheck(*(argv+4));
	    	} else {
				fRead = stdin;
	    	}

	    	// Create write files to write share.1.pbm and share.2.pbm
	    	char wfile[strlen(name) + 6];
			sprintf(wfile, "%s.1.pbm", name);
			FILE *fWrite1 = fopen(wfile, "w");
			sprintf(wfile, "%s.2.pbm", name);
			FILE *fWrite2 = fopen(wfile, "w");
			
			// Add P4 to the file (format of the pbm file)
	    	fread(wfile, 3, 1, fRead);
	    	fprintf(fWrite1, "P4\n");
	    	fprintf(fWrite2, "P4\n");

	    	// Add dimensions of the file
	    	char dimension[5] = "", input;
	    	int i = 0;
	    	do {
	    		fread(&input, 1, 1, fRead);
	    		dimension[i] = input;
	    		i++;
	    	} while (input >= 48 && input <= 57);
	    	int row_size = atoi(dimension);
	    	i=0;
	    	do {
	    		fread(&input, 1, 1, fRead);
	    		dimension[i] = input;
	    		i++;
	    	} while (input >= 48 && input <= 57);

	    	int bytes, rows = atoi(dimension);
	    	fprintf(fWrite1, "%d %d\n", row_size*2, rows*2);
	    	fprintf(fWrite2, "%d %d\n", row_size*2, rows*2);

	    	if (row_size % 8 != 0) {
	    		bytes = (row_size/8) + 1;
	    	} else {
	    		bytes = (row_size/8);
	    	}

	    	int key_index = 7, input_rows = 0;
	    	unsigned char *key = generate_key(pphrase, (bytes * rows)), key_byte = *key;
	    	while (input_rows < rows) {
	    		// Read one row of data and generate share1 and share2.
	    		// store share1 and share2 data in the string which is 4 times the size of row.
	    		// Print this string in the file after a single row iteration.
	    		unsigned char share1[row_size*4 + 1], share2[row_size*4 + 1], pixels[bytes+1];
	    		int i = 0, seek = 0;
	   			fread(pixels, bytes, 1, fRead);
	   			share1[row_size*4 + 1] = '\0';
	   			share2[row_size*4 + 1] = '\0';
	   			while (i<row_size) {
    				unsigned char pixel_byte = pixels[i/8];
    				int k = 7;
    				//printf("At start i:%d\n", i);
    				while (k >= 0) {
	    				if (i == row_size) break;
	    				if (key_index < 0) {
	    					key++;
	   						key_byte = *key;
	    					key_index = 7;
	    				}
	    				int pixel_bit = (pixel_byte >> k) & 0x01;
	    				int key_bit = (key_byte >> key_index) & 0x01;
	    				key_index--;
	    				if (!key_bit) {
	    					memcpy(&share1[i*2], "10", 2);
	    					memcpy(&share1[row_size*2 + i*2], "01", 2);
						} else {
							memcpy(&share1[i*2], "01", 2);
	    					memcpy(&share1[row_size*2 + i*2], "10", 2);
						}

						if (!pixel_bit && !key_bit) {
							memcpy(&share2[i*2], "10", 2);
							memcpy(&share2[row_size*2 + i*2], "01", 2);
						} else if(!pixel_bit && key_bit) {
							memcpy(&share2[i*2], "01", 2);
							memcpy(&share2[row_size*2 + i*2], "10", 2);
						} else if (pixel_bit && !key_bit) {
							memcpy(&share2[i*2], "01", 2);
							memcpy(&share2[row_size*2 + i*2], "10", 2);
						} else {
							memcpy(&share2[i*2], "10", 2);
							memcpy(&share2[row_size*2 + i*2], "01", 2);
						}
	    				i++;
	    				k--;
	    				seek += 2;
	    			}
	    			//printf("At end i:%d\n", i);
	    		}
	    		int r = 0;
	    		for (i=0; i < (row_size*4);) {
	    			int j = 7;
	    			unsigned int s1 = 0, s2 = 0;
	    			for (;j>=0; j--) {
	    				if (r == row_size * 2) {
	    					r = 0;
	    					break;
	    				}
	    				if (!share1[i]) share1[i] = '0';
	    				if (!share2[i]) share2[i] = '0';
	    				s1 += (share1[i] == '1') ? pow(2, j) : 0;
	    				s2 += (share2[i] == '1') ? pow(2, j) : 0;
	    				i++;
	    				r++;
	    			}
					if ( !(row_size%4 == 0 && j >= 0) ) {
	    				fprintf(fWrite1, "%c", s1);
	    				fprintf(fWrite2, "%c", s2);
					}
	    		}
	    		input_rows++;
	    	}
		free(key);
	    	fclose(fRead);
	    	fclose(fWrite1);
	    	fclose(fWrite2);
	} else if ( strcmp(option, "merge") == 0 ) {
			if (argc != 4) pusage();
	    	FILE *fh1, *fh2;
	    	fh1 = fileCheck(*(argv+2));
	    	fh2 = fileCheck(*(argv+3));

	    	// Read byte wise from both the files.
	    	// If both bits taken from same position of 2 files is 0 output is 0 otherwise it's 1
	    	while(!feof(fh1) && !feof(fh2)) {
	    		char input1;
	    		char input2;
	    		int output = 0, i;
	    		fread(&input1, 1, 1, fh1);
	    		fread(&input2, 1, 1, fh2);
	    		if (feof(fh1) || feof(fh2)) break;

	    		for (i = 7; i>= 0; i--) {
	    			int bit1 = (input1>>i) & 0x01;
	    			int bit2 = (input2>>i) & 0x01;
	    			if (bit1 == 1 || bit2 == 1) {
	    				output += pow(2,i);
	    			}
	    		}
	    		fprintf(stdout, "%c", output);
	    	}
	    	fclose(fh1);
	    	fclose(fh2);
    } else {
    		// Check input file for existence and permissions
	    	FILE *fh;
	    	if ( argc == 3 ) {
				fh = fileCheck(*(argv+2));
			} else {
				fh = stdin;
	    	}
	    	
	    	// Add P4 and dimensions to the output
	    	char dimension[5] = "", temp;
	    	fread(dimension, 3, 1, fh);
	    	fprintf(stdout, "%s", dimension);
	    	strcpy(dimension, "");

	    	int i=0;
	    	do {
	    		fread(&temp, 1, 1, fh);
	    		dimension[i] = temp;
	    		i++;
	    	} while (temp >= 48 && temp <= 57);
	    	int row_size = atoi(dimension);

	    	i=0;
	    	do {
	    		fread(&temp, 1, 1, fh);
	    		dimension[i] = temp;
	    		i++;
	    	} while (temp >= 48 && temp <= 57);
	    	int rows = atoi(dimension);
		if (row_size % 2 != 0 || rows %2 != 0) {
			fprintf(stderr, "Input file not in right format\n");
			exit(1);
		} 
	    	fprintf(stdout, "%d %d\n", row_size/2, rows/2);

	    	int bytes;
	    	if  (row_size%8 == 0) {
	    		bytes = row_size/8;
	    	} else {
	    		bytes = (row_size/8) + 1;
	    	}

	    	// If 2 consecutive bits of merged file are 1 then output is 1
	    	for ( i = 0; i < rows; i = i+2) {
	   			char input[bytes+1];
	   			fread(input, bytes, 1, fh);
	   			int r = 0, j = 7;
	   			unsigned int output = 0;
	   			for ( ;r < row_size; r =r+2 ) {
		   			if (j < 0 ) {
	   					fprintf(stdout, "%c", output);
	   					j = 7;
	   					output = 0;
	   				}
	   				int index = (7 - r%8);
	    			int bit1 = (input[r/8]>>index) & 0x0001;
	    			int bit2 = (input[r/8]>>(index -1)) & 0x0001;
	    			if (bit1 == 1 && bit2 == 1) output += pow(2, j);
	   				j--;
  				}
  				fprintf(stdout, "%c", output);
  				fread(input, bytes, 1, fh);
  			}
	    	fclose(fh);
     }
     return (0);
}
